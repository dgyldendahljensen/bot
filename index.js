require('dotenv').config();
const Discord = require("discord.js")
const client = new Discord.Client()

const fetch = require('node-fetch');
const querystring = require('querystring');

const prefix = '!';

const serverGuildIdMap = [
	{
		"name": "Mirage Raceway",
		"ids": [
			"690997663960662078", // Enigma
			"718524443407941774" // Gyldendahl's server
		]
	},
	{
		"name": "Pyrewood Village",
		"ids": [
			"238730373046075392" // DMC
		]
	}
];

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`)
});

client.on('message', async message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(/ +/);
	const command = args.shift().toLowerCase();

	if (command === 'pp-search' || command === 'pp-exactsearch') {

		// Making sure that the message is not coming from DMChannel 
		// as we cannot check what wow server we need to return a result from.
		if (message instanceof Discord.DMChannel) {
			return message.channel.send('You cannot search through DMs as the result depends on the server id');
		}

		if (!args.length) {
			return message.channel.send('You need to supply a search term!');
		}

		let serverName = "";
		
		serverGuildIdMap.forEach(server => {
			for (i = 0; i < server.ids.length; i++) {
				if (server.ids[i] === message.guild.id) {
					serverName = server.name;
				}
			}
		});

		if (!serverName.length) {
			return message.channel.send('Could not find the classic wow server name to search');
		}

		const isExact = command === 'pp-exactsearch';
		let query = process.env.URL + 'search?' + querystring.stringify({ query: args.join(' ') }) + '&' + querystring.stringify({ server: serverName });
		if (isExact) {
			query = query + '&exact=true';
		}

		const result = await fetch(query).then(response => response.json());

		if (result.players !== undefined) {

			const { players } = result;

			const embed = new Discord.MessageEmbed()
				.setColor('#EFFF00')
				.setTitle(result.recipe.name)
				.setURL('https://classic.wowhead.com/spell=' + result.recipe.id);

			embed.addField('Crafter(s)', players.map(element => element.name).join(', '), true);

			return message.channel.send(embed);

		} else if (result.list !== undefined) {

			const { list } = result;

			const embed = new Discord.MessageEmbed()
				.setColor('#EFFF00')
				.setTitle('Top results(max 5) for: ' + args.join(' '))
			list.forEach(element => {
				embed.addField('Item', element.name, true);
			});

			return message.channel.send(embed);

		} else if (result.recipe !== undefined) {

			const recipe = result.recipe;

			const embed = new Discord.MessageEmbed()
				.setColor('#EFFF00')
				.setTitle('No crafters found for: ' + result.recipe.name)
				.setURL('https://classic.wowhead.com/spell=' + result.recipe.id);

			return message.channel.send(embed);

		}

		if (!result.length) {
			return message.channel.send(`No results found for **${args.join(' ')}**.`);
		}

	} else if (command === 'pp-upload') {
		const uploadBody = args.join(' ');

		const result = await fetch(process.env.URL + 'upload', { method: 'POST', body: uploadBody }).then(response => response.text());
		return message.channel.send('Upload has been executed');

	} else if (command === 'pp-help') {
		return message.channel.send('Commands: !pp-help, !pp-search, !pp-exactsearch, !pp-upload');
	}
});

client.login(process.env.TOKEN);